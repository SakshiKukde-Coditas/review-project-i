import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import {  MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatOptionModule } from "@angular/material/core";

const Material = [
    MatFormFieldModule,MatTableModule, MatTabsModule,MatDialogModule,MatCardModule, MatInputModule, MatSelectModule,MatTreeModule,MatIconModule, MatProgressSpinnerModule,MatOptionModule
]
@NgModule({
    imports: [Material],
    exports: [Material],
  })
  export class AngularMaterialModule {}