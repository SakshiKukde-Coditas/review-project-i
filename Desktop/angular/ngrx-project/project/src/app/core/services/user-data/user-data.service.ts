import { Injectable } from '@angular/core';
import { RootReducerState } from 'src/app/reducers';
import { Store } from '@ngrx/store';
import { getUsers, getUserLoading, getUserLoaded, getUserError } from 'src/app/reducers/index';
import { Observable, combineLatest, take } from 'rxjs';
import { UserDeleteAction, UserListErrorAction, UserListRequestAction, UserListSuccessAction, UserUpdateAction } from 'src/app/actions/user-action';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(private userService: UserService,private store: Store<RootReducerState>) { }

  getAllUsers(force = false):[Observable<boolean>,Observable<any[]>,Observable<boolean>] {
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const getUserData$ = this.store.select(getUsers);
    const getError$ = this.store.select(getUserError);

    combineLatest([loaded$,loading$]).pipe(take(1)).subscribe((data)=>{

      if((!data[0] && !data[1]) || force){
        this.store.dispatch(new UserListRequestAction());
        console.log("inside get users")
        this.userService.getUsers().subscribe({
          next: (response: any) => {
            this.store.dispatch(new UserListSuccessAction({data: response}));
          },
          error:(error:any)=>{
           this.store.dispatch(new UserListErrorAction());
          }
        })
      } 
    })
    return [loading$,getUserData$, getError$]; 
  }

  deleteUser(id:number,force = false):[Observable<boolean>,Observable<any[]>,Observable<boolean>] {
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const getUserData$ = this.store.select(getUsers);
    const getError$ = this.store.select(getUserError);

    combineLatest([loaded$,loading$]).pipe(take(1)).subscribe((data)=>{
      if((data[0] || force)){
        
        console.log("inside delete users")
        this.userService.deleteUser(id).subscribe({
          next: (response: any) => {
            console.log(response);
            // this.store.dispatch(new UserListSuccessAction({data: response}));
            this.store.dispatch(new UserDeleteAction({id}));
            this.getAllUsers();
          },
          error:(error:any)=>{
           this.store.dispatch(new UserListErrorAction());
          }
        })
      } 
    })
    return [loading$,getUserData$, getError$]; 
  //   this.userService.deleteUser(id).subscribe({
  //     next: (response: any) => {
  //       console.log(response);
  //     },
  //     error:(error:any)=>{
  //       console.log(error);
  //     //  this.store.dispatch(new UserListErrorAction());
  //     }
  //   })
    
  }

  updateUser(data:any):[Observable<boolean>,Observable<any[]>,Observable<boolean>] {
    console.log(data);
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const getUserData$ = this.store.select(getUsers);
    const getError$ = this.store.select(getUserError);

    combineLatest([loaded$,loading$]).pipe(take(1)).subscribe((res)=>{
      console.log(res,"data");
      if((res[0])){
        console.log("inside update users")
        console.log(data);
        this.userService.updateUser(data).subscribe({
          next: (response: any) => {
            console.log(response);
            this.store.dispatch(new UserUpdateAction({data}));
          },
          error:(error:any)=>{
           this.store.dispatch(new UserListErrorAction());
          }
        })
      } 
    })
    return [loading$,getUserData$, getError$]; 

  }

  addUser(data:any):[Observable<boolean>,Observable<any[]>,Observable<boolean>] {
    console.log(data);
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const getUserData$ = this.store.select(getUsers);
    const getError$ = this.store.select(getUserError);

    combineLatest([loaded$,loading$]).pipe(take(1)).subscribe((res)=>{
      console.log(res,"data");
      if((res[0])){
        console.log("inside add users")
        console.log(data);
        this.userService.addUser(data).subscribe({
          next: (response: any) => {
            console.log(response);
            this.store.dispatch(new UserUpdateAction({data}));
          },
          error:(error:any)=>{
           this.store.dispatch(new UserListErrorAction());
          }
        })
      } 
    })
    return [loading$,getUserData$, getError$]; 

  }
}
