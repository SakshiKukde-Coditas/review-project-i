
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient){}

  getUsers() {
    return this.http.get( 'http://localhost:3000/user/getAllUsers');
  }
  // https://8107-115-160-223-174.in.ngrok.io/api/getAllCricketPlayers
  addUser(reqBody:any) {
    console.log(reqBody,'inisde service');
    
    return this.http.post<any>(`http://localhost:3000/user/addUser`,reqBody);
  }

  deleteUser(value:any) {
    console.log("inside dlete user", value);
    return this.http.delete<any>(`http://localhost:3000/user/deleteUser`,value);
  }

  updateUser(reqBody:any) {
    console.log("inside update user", reqBody);
    return this.http.put<any>(`http://localhost:3000/user/updateUser`,reqBody);
  }
}
