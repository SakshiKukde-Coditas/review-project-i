import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from 'src/app/core/services/language/language.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginEmail!: string;
  loginPassword!: string;
  signupEmail!: string;
  signupPassword!: string;
  showSignupForm: boolean = false;
  selectedLanguage!: string;
  showPassword: boolean = false;

  constructor(private router: Router, private translate: TranslateService, private languageService: LanguageService) {
    translate.setDefaultLang('en');
    this.selectedLanguage = this.languageService.selectedLanguage;
  }

  login() {
    this.translate.setDefaultLang('en');
    if(this.loginEmail === 'sakshi.kukde@coditas.com' && this.loginPassword === 'saakshi@26') {
      this.router.navigate([`users`]);
    }
  }

  changeLanguage(language: string) {
    console.log(language,"niside change language");
    this.selectedLanguage = language;
    this.translate.use(language);
    // this.languageService.setLanguage(language);
  }
}
