import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './pages/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
		path: 'auth',
		component: AuthComponent,
		// canActivate:[AuthGuard],
		children: [
			{
				path: '',
				pathMatch: 'full',
				redirectTo: 'auth/login',
			},
			{ path: 'login', component: LoginComponent }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
