import { ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-change-detection',
  templateUrl: './change-detection.component.html',
  styleUrls: ['./change-detection.component.scss']
})
export class ChangeDetectionComponent {

  isActiveParent = false;
  activeChildNode: string = '';
  isActiveChild = false;
  
  constructor(private cdr: ChangeDetectorRef) {}
  
  toggleNode(node: string): void {
    if (node === 'parent') {
      this.isActiveParent = !this.isActiveParent;
      this.cdr.detectChanges();
    } else if (node === 'child1' || node === 'child2') {
      this.activeChildNode = node;
      this.cdr.detectChanges();
    }
  }
  
  isActive(node: string): boolean {
    return this.isActiveParent;
  }
  
  isChildActive(childNode: string): boolean {
    return this.activeChildNode === childNode;
  }

}


