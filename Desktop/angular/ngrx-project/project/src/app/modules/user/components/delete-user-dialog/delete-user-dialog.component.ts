import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserDataService } from 'src/app/core/services/user-data/user-data.service';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.scss']
})
export class DeleteUserDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public message: any,
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>, private userDatService: UserDataService
  ) {}

  closeDialog(action: string) {
    console.log(action);
    if(action === 'no') {
      this.dialogRef.close(action);
    }
    else if(action === 'yes') {
      this.dialogRef.close(action);
    }

	} 

}
