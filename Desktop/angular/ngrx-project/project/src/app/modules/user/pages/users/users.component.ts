import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AddUserDialogComponent } from '../../components/add-user-dialog/add-user-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnChanges{

  @Input() user:any=[]; 
  displayedColumns: string[] = ['id', 'name', 'email', 'number', 'edit', 'delete'];
  id!:number;
  data:any = {}
  @Output() userId = new EventEmitter();
  @Output() userData = new EventEmitter();
  constructor(private dialog: MatDialog){}

  ngOnChanges(): void {
  }

  edit(user:any){
    console.log(user);
    this.data = user;
    this.userData.emit(user);
  }

  delete(id: number) {
    this.id = id;
    this.userId.emit(id);
  }

  add(value:any) {
    const data = {
      operation: value
    }
    console.log(data);
    this.userData.emit(data);
  }
}
