import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserRoutingModule } from '../../user-routing.module';
import { UserService } from 'src/app/core/services/user/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit{

  initialUpdateValues: any = {
		id: '',
		name: '',
		age: '',
		nickName: ''
	}

  userForm = new FormGroup({
		id: new FormControl(),
		name: new FormControl('', [Validators.required, Validators.maxLength(20), Validators.pattern('.*[^ ].*')]),
		email: new FormControl(),
		number: new FormControl('', Validators.required)
	});

  constructor(@Inject(MAT_DIALOG_DATA) public data:any,public dialogRef: MatDialogRef<AddUserDialogComponent>, private userService: UserService){}

  ngOnInit(): void {
    console.log(this.data);
    if(this.data.operation === 'edit') {
      this.userForm.patchValue(this.data);
      this.initialUpdateValues.id = this.data.id;
			this.initialUpdateValues.name = this.data.name;
			this.initialUpdateValues.email = this.data.email;
			this.initialUpdateValues.number = this.data.number;
    }
  }

  submit() {
    console.log('inside submit');
    if(this.data.operation === 'add') {
     console.log(this.userForm.value);
     const payload = {
      name: this.userForm.value.name,
      email: this.userForm.value.email,
      number: this.userForm.value.number
     }
      this.dialogRef.close(payload);
    }
  else if(this.data.operation === 'edit'){
    let userData: any = {
      id: this.userForm.value.id,
      name: this.userForm.value.name,
      email: this.userForm.value.email,
      number: this.userForm.value.number
    }
    console.log(userData);
    this.dialogRef.close(userData);
  }
}
Cancel() {
  this.dialogRef.close();
}
}
